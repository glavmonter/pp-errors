# Ошибки ParkPass #

Команда DISCONNxxx приходит только после успешного подключения к устройству в конце обмена или при ошибках.
xxx - числовой код ошибки.

Коды ошибок:

| Код | Описание | Версия |
|:---:| :-----| :---:|
| 0   | Таймаут ответа на HELLO |  |
| 1   | Вышли все попытки установить связь со стойкой, ожидание CONNOK |  |
| 2   | Таймаут получения данных по BLE |  |
| 3   | Таймаут получения команды UADAT по BLE |  |
| 4   | Таймаут UART_DATA_REPLY |  |
| 5   | Ошибка генерирования пары ключей |  |
| 6   | Ошибка кодирования открытого ключа в Base64 |  |
| 7   | Ответ стойки CONNOK не верный |  |
| 8   | Ответ стойки GETPC не верный |  |
| 9   | Переполнение приемного буфера UART |  |
| 10  | Ошибка генерирования общего ключа |  |
| 11  | Ответ OLLEH не верный |  |
| 12  | Переполнение буфера BLE |  |
| 13  | Ответ UADAT не верный |  |
| 14  | Ответ OK не верный |  |
| 15  | Выдача ответа DATA_OK на стойку, окончание обмена |  |
| 16  | Ошибка AES, переполнение буфера |  |
| 17  | AES CRYPTO_NOT_INITIALIZED      |  |
| 18  | CRYPTO_CONTEXT_NULL             |  |
| 19  | CRYPTO_CONTEXT_NOT_INITIALIZED  |  |
| 20  | CRYPTO_FEATURE_UNAVAILABLE      |  |
| 21  | CRYPTO_BUSY                     |  |
| 33  | CRYPTO_INPUT_NULL               |  |
| 34  | CRYPTO_INPUT_LENGTH             |  |
| 35  | CRYPTO_INPUT_LOCATION           |  |
| 36  | CRYPTO_OUTPUT_NULL              |  |
| 37  | CRYPTO_OUTPUT_LENGTH            |  |
| 38  | CRYPTO_ALLOC_FAILED             |  |
| 39  | CRYPTO_INTERNAL                 |  |
| 40  | CRYPTO_INVALID_PARAM            |  |
| 41  | CRYPTO_KEY_SIZE                 |  |
| 42  | CRYPTO_STACK_OVERFLOW           |  |
| 81  | CRYPTO_ECC_KEY_NOT_INITIALIZED  |  |
| 82  | CRYPTO_ECDH_CURVE_MISMATCH      |  |
| 83  | CRYPTO_ECDSA_INVALID_SIGNATURE  |  |
| 84  | CRYPTO_ECC_INVALID_KEY          |  |
| 97  | CRYPTO_AES_INVALID_PADDING      |  |
| 113 | CRYPTO_AEAD_INVALID_MAC         |  |
| 114 | CRYPTO_AEAD_NONCE_SIZE          |  |
| 115 | CRYPTO_AEAD_MAC_SIZE            |  |
| 129 | CRYPTO_RNG_INIT_FAILED          |  |
| 130 | CRYPTO_RNG_RESEED_REQUIRED      |  |
| 255 | Отключение от стека BLE. Или телефон отключился или ридер |  |

Номера ошибок в будущих версиях будут добавлятся.

## Расширенное описание ошибки 15 и 255
Часть кода, отвечающая за PP, при **нормальном** завершении обмена данными отправляет на стойку **DATA_OK**, означающий прием и передачу всех данных и **DISCONN15**, означающий начало отключения считывателя от телефона. В момент **DISCONN15** сигнал отключения уже передан в стек BLE и в самом финале стек BLE вызывает функцию on_disconnect из которой уже посылается **DISCONN255**. В сумме сигнал 15 и следующий за ним 255 являются верным завершением работы.